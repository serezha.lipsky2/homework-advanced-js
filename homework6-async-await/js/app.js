async function calculateIP(){
const root = document.getElementById("root");
const calcBtn = document.createElement("button");
root.append(calcBtn);
calcBtn.innerText = "Вычислить по IP"
calcBtn.addEventListener("click", async()=>{
    await fetch("https://api.ipify.org/?format=json")
    .then(data =>data.json())
    .then(userIP =>{
        console.log(userIP.ip);
        fetch("http://ip-api.com/json/" + userIP.ip).then(data=> data.json()).then(res=>{
            console.log(res);
            const ul = document.createElement("ul");
            const li = document.createElement("li");
            li.innerHTML = `<strong>Континент: ${res.timezone}</strong> <br> 
            <strong>Страна: ${res.country}</strong><br>
            <strong>Регион: ${res.regionName}</strong><br>
            <strong>Город: ${res.city}</strong><br>
            <strong>Район: ${res.region}</strong> `
            ul.append(li)
            root.append(ul)
        })
    })
})
}
calculateIP()