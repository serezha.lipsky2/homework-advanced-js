const usersUrl = "https://ajax.test-danit.com/api/json/users";
const postsUrl = "https://ajax.test-danit.com/api/json/posts";



let root = document.getElementById("root");
root.classList.add("container")


let ul = document.createElement("ul");
let post;
class Card {
	constructor(usersUrl, postsUrl) {
		this.usersUrl = usersUrl;
		this.postsUrl = postsUrl;
	}
	creator() {
		fetch(this.usersUrl)
			.then(response => response.json())
			.then(users => {
				



				fetch(this.postsUrl)
					.then(response => response.json())
					.then(data => {

						data.forEach(post => {
							for (const key of users) {
								console.log(post);
								const delBtn = document.createElement('button');
								const ul = document.createElement('ul')
								if(post.userId === key.id){
								  ul.innerHTML = `<strong>Name: ${key.name}</strong> <i>Email: ${key.email}</i><br><strong>Title: ${post.title}</strong><br>${post.body}<br>`
								  delBtn.innerText = 'delete post'
								  ul.append(delBtn)
								  root.append(ul)
								}
								delBtn.addEventListener("click", () =>{
									fetch(this.postsUrl + "/" + post.userId, {method:"DELETE"}).then(res =>{
										ul.remove()
									})
								})
							}	
						
						});
					});
			});
	}

}

const cardUser = new Card(usersUrl, postsUrl);
cardUser.creator();

