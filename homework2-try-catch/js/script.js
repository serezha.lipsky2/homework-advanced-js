"use strict"

const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];
  
  const root = document.querySelector('#root');

class Books{
    constructor(arr){
        this.arr = arr;
    }
 addList(){
  const ul = document.createElement('ul');
  const createList = ({author, name, price}) => {
    const li = document.createElement("li");
    li.innerHTML = `<ul><li>Автор: <b>${author}</b></li><li>Название: <i>${name}</i></li><li>Цена: ${price}</li></ul>`;
    ul.append(li);
    root.append(ul)
}
this.arr.forEach((el) => {
  try {
    if (!el.author) {
      throw new Error("Не указан автор");
  }
    if (!el.name) {
      throw new Error("Без названия");
  }
    if (!el.price) {
      throw new Error("Нет цены");
    }
    createList({
      author: el.author,
      name: el.name,
      price: el.price
  });
  } catch (err) {
    console.log(el)
    console.log(err);
  }
        });
      }}
      const list = new Books(books);
list.addList()
console.log(list);
