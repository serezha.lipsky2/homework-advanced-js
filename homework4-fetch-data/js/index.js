"use strict"
const root = document.querySelector("#root");
root.style.background = "black"
const ul  = document.createElement("ul");
class Films{
constructor(url){
this.url = url
}
getListFilms(){
    fetch(this.url)
    .then(response => response.json())
    .then(data => {
        data.forEach(element => {
            const actors = element.characters;
            for (const key of actors) {
                fetch(key)
                .then(res => res.json())
                .then(dat => {
                    const p = document.createElement("p");
                    p.innerHTML = `<strong>Name Actor:</strong> ${dat.name}`
                    p.style.color = "green"
                    li.append(p);
                })
            }
            const li = document.createElement("li");
            li.innerHTML = `<strong>Name Film:</strong> ${element.name} <br> <strong>Episode:</strong> ${element.episodeId}<br> <strong>Opening:</strong> ${element.openingCrawl}`
            li.style.color = "gold"
            ul.append(li);
            root.append(ul);
        });
    })
}
}
const films = new Films ("https://ajax.test-danit.com/api/swapi/films");
films.getListFilms();