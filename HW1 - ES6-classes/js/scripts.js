"use strict"
class Employee{
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name(){
        return this.name;
    }
    set name (value){
        this._name = value;
    }
    get age(){
        return this.age;
    }
    set age (value){
        this._age = value;
    }
    get salary(){
        return this.salary;
    }
    set salary (value){
        this._salary = value;
    }
}
class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang.join(", ");
    }
    set lang(value){
        this._lang = value;
    }
    get lang(){
        return this.lang;
    }
    set salary(value){
        this._salary = value * 3;
    }
    get salary(){
        return this.salary;
    }
}
let programmer1 = new Programmer("Vasya", 18, 2000, ["Russian", "English", "France"]);
console.log(programmer1);
let programmer2 = new Programmer("Sveta", 21, 4000, ["Russian", "English", "France", "German", "indian"]);
console.log(programmer2);
let programmer3 = new Programmer("Maks", 25, 7000, ["Russian", "English", "France", "German", "indian", "Ukrainian", "Sweden"]);
console.log(programmer3);