"use strict"
// task 1 

function client(){
    const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
    const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
    const unit = [...clients1, ...clients2];
    console.log(unit);
}
client()
// task 2
function char(){
    const characters = [
        {
          name: "Елена",
          lastName: "Гилберт",
          age: 17, 
          gender: "woman",
          status: "human"
        },
        {
          name: "Кэролайн",
          lastName: "Форбс",
          age: 17,
          gender: "woman",
          status: "human"
        },
        {
          name: "Аларик",
          lastName: "Зальцман",
          age: 31,
          gender: "man",
          status: "human"
        },
        {
          name: "Дэймон",
          lastName: "Сальваторе",
          age: 156,
          gender: "man",
          status: "vampire"
        },
        {
          name: "Ребекка",
          lastName: "Майклсон",
          age: 1089,
          gender: "woman",
          status: "vempire"
        },
        {
          name: "Клаус",
          lastName: "Майклсон",
          age: 1093,
          gender: "man",
          status: "vampire"
        }
      ];
class Characters{
    constructor(characters){
        this.characters = characters;
    }
   create(){
    const arr =  Array.from(this.characters);
    for (const key of arr) {
        delete key["gender"];
        delete key["status"];
        console.log(key);
    }
}
}   
const charactersShortInfo = new Characters(characters) ;
charactersShortInfo.create()
}
char()
// Task 3
function user(){
    const user1 = {
        name: "John",
        years: 30
      };
      
    const{name, years, isAdmin = false} = user1;
    console.log(name, years, isAdmin);
}
user()
// Task 4 
function dossier(){
    const satoshi2020 = {
        name: 'Nick',
        surname: 'Sabo',
        age: 51,
        country: 'Japan',
        birth: '1979-08-21',
        location: {
          lat: 38.869422, 
          lng: 139.876632
        }
      }
      
      const satoshi2019 = {
        name: 'Dorian',
        surname: 'Nakamoto',
        age: 44,
        hidden: true,
        country: 'USA',
        wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
        browser: 'Chrome'
      }
      
      const satoshi2018 = {
        name: 'Satoshi',
        surname: 'Nakamoto', 
        technology: 'Bitcoin',
        country: 'Japan',
        browser: 'Tor',
        birth: '1975-04-05'
      }
const fullProfile = Object.assign(satoshi2018, satoshi2019, satoshi2020);
console.log(fullProfile);   
}
dossier()
// task 5 
function harryPotter(){
    const books = [{
        name: 'Harry Potter',
        author: 'J.K. Rowling'
      }, {
        name: 'Lord of the rings',
        author: 'J.R.R. Tolkien'
      }, {
        name: 'The witcher',
        author: 'Andrzej Sapkowski'
      }];
      
      const bookToAdd = {
        name: 'Game of thrones',
        author: 'George R. R. Martin'
      }
books.push(bookToAdd);
 let potter = [...books]
 console.log(potter);      
}
harryPotter()
// Task 6
function boxer(){
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }
const boxerInfo = {age: 18,
     salary: 10000};
     let boxerEmployee = {...employee, ...boxerInfo}
console.log(boxerEmployee);
}
boxer()
// Task 7
function shows(){
    const array = ['value', () => 'showValue'];

// Допишите ваш код здесь
const [value, showValue] = array

alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'

}
shows()
